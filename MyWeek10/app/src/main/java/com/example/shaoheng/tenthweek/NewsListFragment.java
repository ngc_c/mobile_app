package com.example.shaoheng.tenthweek;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;


public class NewsListFragment extends Fragment {

    //自定义一个接口，将用于给activity传递一个listview点击事件
    //更详细的原理，教材P148"3.与Activity通信"处总结得很好，可在实践前与实践后多次看
    public interface OnNewsSelectedListener {
        void onNewsSelected(Bundle bundle);
    }

    //定义一个自定义接口的对象
    private OnNewsSelectedListener mOnNewsSelectedListener;

    //定义空的构造函数
    public NewsListFragment() {
        //Required empty public constructor
    }
    //当引用Fragment生命周期的onAttach()方法时，就自动获取MainActivity的上下文，所以不用再通过该方法传递上下文了
     public void setOnNewsSelectedListener(OnNewsSelectedListener OnNewsSelectedListener){
     mOnNewsSelectedListener=OnNewsSelectedListener;
    }

    //当Fragment对象关联到一个上下文（常为Activity）时，这个方法将被调用
    //调用onAttach()后才调用onCreate()
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //若context是OnNewsSelectedListener的实例，即实现了OnNewsSelectedListener接口，
        // 则使内部对象onNewsSelectedListener获得该context（在后面onCreate方法执行时才不为空）
        // 所以，要求要整合这个Fragment的Activity必须implement（实现）OnNewsSelectedListener接口
        if (context instanceof OnNewsSelectedListener) {
            mOnNewsSelectedListener = (OnNewsSelectedListener) context;
        } else {
            throw new IllegalArgumentException("ActivitymustOnNewsSelectedListener");
        }
    }
//当Fragment对象从上下文（常为Activity）中删除时被调用
    @Override
    public void onDetach(){
        mOnNewsSelectedListener=null;
        super.onDetach();
    }
    //当Fragment对象创建时调用（已经成功执行onAttach）
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        //为这个fragment找到对应的布局，inflate--充气、膨胀
        // inflate的作用是将一个layout.xml布局文件找出来，将将其变为一个View对象
        // 对下面inflate()方法的进一步解释，见本方法定义之后的块注释
        View ret=inflater.inflate(R.layout.fragment_news_list,container,false);

        //定义ListView列出新闻标题，这个listView实例要从xml布局文件中的指定控件
        // 这里要的是fragment_news_list.xml中的news_list
        ListView listView=(ListView)ret.findViewById(R.id.news_list);

        //设置点击列表项的事件监听器
        listView.setOnItemClickListener(listener);

        //设置新闻列表内容的匹配器
        if(listView!=null){
            //为简化系统，重点落在解决方案的机制上，这里“生成”新闻，是十足的假新闻
            // 是否动心构思一个用文件系统或数据库提供新闻服务的方案？定个目标，做一做吧
            ArrayList<String> data=new ArrayList<>();
            for(int i=0;i<100;i++){
                data.add("新闻标题"+((i<10)?"0"+i:i));
            }
            //定义适配器，并指定到新闻列表中
            ArrayAdapter<String> mAdapter=new ArrayAdapter<String>(
                    getContext(),android.R.layout.simple_list_item_1,
                    data
                    );
            listView.setAdapter(mAdapter);
        }
        return ret;
    }
    //设置处理列表项点击的事件监听器
    private AdapterView.OnItemClickListener listener=new AdapterView.OnItemClickListener(){
        @Override
                public void onItemClick(AdapterView<?>parent,View view,int position,long id){
            //当Fragment对象创建时调用（这一点能保证，因为已经成功执行了onAttach）
            if(mOnNewsSelectedListener!=null){
                Bundle bundle=new Bundle();
                bundle.putInt("position",position);
                bundle.putLong("id",id);
                //向自定义事件监听器传送新闻
                mOnNewsSelectedListener.onNewsSelected(bundle);
            }
        }
    };
}