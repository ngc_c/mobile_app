package com.example.shaoheng.tenthweek;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

public class TabHostActivity extends AppCompatActivity {

    //定义TabHost对象
    TabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_host);
        //获取该Activity里面的TabHost组件
        tabHost= (TabHost) findViewById(R.id.tabHost);
        //初始化TabHost容器
        tabHost.setup();

        //定义Tab页对象
        TabHost.TabSpec spec1,spec2,spec3;
        //创建第一个Tab页：学习场所
        //利用setIndicator可以设文字标题和图标，但仅当文字为空时图标才显示
        spec1=tabHost.newTabSpec("tab1").
                setIndicator("",getResources().getDrawable(R.drawable.learningsite)).
                setContent(R.id.content_1);
        //添加第一个标签页
        tabHost.addTab(spec1);

        //创建第一个Tab页
        spec2=tabHost.newTabSpec("tab2").
                setIndicator("",getResources().getDrawable(R.drawable.campus)).
                setContent(R.id.content_2);
        tabHost.addTab(spec2);


        //创建第一个Tab页
        spec3=tabHost.newTabSpec("tab3").
                setIndicator("要点提示",getResources().getDrawable(R.drawable.keypoint)).
                setContent(R.id.content_3);
        tabHost.addTab(spec3);

        //设置选项卡改变时的事件监听器
        tabHost.setOnTabChangedListener(onTabChangedListener);
    }

    private void setTab1Business(){
        final String[]learningSiteArray={"逸夫图书馆","承先图书馆","综合楼","四教","五教","六教"};
        ListView listView1;
        //获取系统默认的ListView组件
        listView1= (ListView) findViewById(R.id.listView1);
        //设置列表视图的数据适配器：外观采用系统提供的布局simple_list_item_1，数据来自自定义数组
        ArrayAdapter<String>adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,
                learningSiteArray);
        listView1.setAdapter(adapter);

        //设置ListView事件监听
        listView1.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?>parent,View view,
                                    int position,long id){
                //这里的处理需要换作有实际应用背景的逻辑
                Toast.makeText(TabHostActivity.this,
                        "您选择了"+learningSiteArray[position],Toast.LENGTH_LONG)
                        .show();
            }
        });
    }
    //标签切换事件处理
    private TabHost.OnTabChangeListener onTabChangedListener
            =new TabHost.OnTabChangeListener(){
        @Override
        public void onTabChanged(String tabId){
            //参数tabId是newTabSpec第一个参数设置的tab页名，并不是layout里面的标识符id
            if(tabId.equals("tab1")){ //第一个标签
                Toast.makeText(TabHostActivity.this,
                        "选择了学习场所,本提示可以换为需要的业务处理",
                        Toast.LENGTH_SHORT)
                        .show();
            }
            if(tabId.equals("tab2")){ //第二个标签
                Toast.makeText(TabHostActivity.this,
                        "选择了校园风光,本提示可以换为需要的业务处理",
                        Toast.LENGTH_SHORT)
                        .show();
            }
            if(tabId.equals("tab2")){ //第三个标签
                Toast.makeText(TabHostActivity.this,
                        "选择了要点提示,本提示可以换为需要的业务处理",
                        Toast.LENGTH_SHORT)
                        .show();

            }
        }
    };
}
