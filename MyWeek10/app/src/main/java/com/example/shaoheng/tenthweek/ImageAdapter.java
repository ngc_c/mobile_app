package com.example.shaoheng.tenthweek;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

/**
 * Created by shaoheng on 2022/5/4.
 */

public class ImageAdapter extends BaseAdapter {
    private Context context;
    //一组Image的Id
    private int[] imageIds;

    public ImageAdapter(Context context,int[] mThumbIds){
        this.context=context;
        this.imageIds=mThumbIds;
    }

    @Override
    public int getCount(){
        return imageIds.length;
    }

    @Override
    public Object getItem(int position){
        return imageIds[position];
    }

    @Override
    public long getItemId(int position){
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
        //定义一个ImageView,显示在GridView里
        ImageView imageView;
        if(convertView==null){
            imageView=new ImageView(context);
            imageView.setLayoutParams(new GridView.LayoutParams(300,300));
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            imageView.setPadding(8,8,8,8);
        }else{
            imageView=(ImageView)convertView;
        }
        imageView.setImageResource(imageIds[position]);
        return imageView;
    }
    /*ViewgetView(intposition,ViewconvertView,ViewGroupparent)
    获取一个显示数据集中指定位置的数据视图。
    第一个参数position---该视图在适配器数据中的位置
    第二个参数convertView---旧视图
    第三个参数parent----此视图最终会被附加到的父级视图
    */

}
