package com.example.shaoheng.tenthweek;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

public class TextBookContextMenuActivity extends AppCompatActivity {

    //声明一个共享参数对象，用于SP读取数据
    private SharedPreferences preferences;
    //声明一个共享参数编辑器对象，用于修改和写入SP数据
    private SharedPreferences.Editor editor;

    //定义组件
    EditText editTitleEdt,editContentEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_book_context_menu);

        //初始化组件
        editTitleEdt= (EditText) findViewById(R.id.editTitleEdt);
        editContentEdt= (EditText) findViewById(R.id.editContentEdt);

        //注册上下文菜单，长按则弹出
        this.registerForContextMenu(editContentEdt);
        //可以在标题框上也注册上下文菜单，查看其效果（自行在下面注册）

        //获取共享参数对象实例，参数分别是文件名和打开方式
        preferences=getSharedPreferences("my_private",MODE_PRIVATE);
        //获得共享参数对象实例编辑器对象
        editor=preferences.edit();
    }
        //定义上下文菜单
    @Override
    /*public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu,v,menuInfo);
        menu.setHeaderTitle("秘密操作");
        menu.add(0,201,1,"找出小秘密");
        menu.add(0,202,2,"保存小秘密");
        menu.add(0,203,3,"退出应用");
    }
    //定义事件监听器,处理各菜单项（子菜单项）被选中时的业务
    @Override
    public boolean onContextItemSelected(MenuItem item){
        //Log.d("提醒",Integer.toString(item.getItemId()))
        //这一部分重复的代码，可以从上一个项目中复制过来
        switch(item.getItemId()){
            case 201: //从SP中找出小秘密
                readPrivacy();
                break;
            case 202: //保存小秘密
                savePrivacy();
                break;
            case 203: //退出处理：
                savePrivacy();
                finish();
            break;
        }
        return super.onContextItemSelected(item);
    }
    */

    public void onCreateContextMenu(ContextMenu menu,View v,ContextMenu.ContextMenuInfo menuInfo){
        super.onCreateContextMenu(menu,v,menuInfo);
        //设置菜单的“标题头”
        menu.setHeaderTitle("秘密操作");
        //用getMenuInflater()获得Activity中的MenuInflater对象后，
        // 利用inflate()方法“膨胀”菜单资源
        // 简单讲，就是将一个用xml定义的菜单资源查找出来，将其与menu关联
        getMenuInflater().inflate(R.menu.menu_textbook,menu);
    }
    //定义事件监听器,处理各菜单项（子菜单项）被选中时的业务
    @Override
    public boolean onContextItemSelected(MenuItem item){
        switch(item.getItemId()){
            case R.id.contextMemuRead: //从SP中找出小秘密
                readPrivacy();
                break;
            case R.id.contextMemuSave: //保存小秘密
                 savePrivacy();
                break;
            case R.id.contextMemuExit: //退出处理
                savePrivacy();
                finish();
                break;
        }
        return super.onContextItemSelected(item);
    }
    //保存小秘密-将文本存入SharedPreferences
    private void savePrivacy(){
        //用编辑器对象完成写入
        // 用putXXX()方法记录各种类型数据，每项数据都有命名和取值，使构成key-value>键值对
        editor.putString("title",editTitleEdt.getText().toString());
        editor.putString("content",editContentEdt.getText().toString());
        //提交编辑器中的修改——一个关键操作
        editor.commit();
    }
    //读取小秘密-取出SP中的数据
    private void readPrivacy(){
        //用getXXX方法按关键字取出SP中的各项值并填入对应编辑框
        editTitleEdt.setText(preferences.getString("title",null));
        editContentEdt.setText(preferences.getString("content",null));
    }
}
