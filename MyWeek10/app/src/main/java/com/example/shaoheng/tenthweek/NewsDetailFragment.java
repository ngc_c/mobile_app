package com.example.shaoheng.tenthweek;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


public class NewsDetailFragment extends Fragment {

    //定义组件
    private TextView newsDetailTxt;
    private TextView newsTitleTxt;

    //定义空的构造函数
    public NewsDetailFragment(){
        //Required empty public constructor
    }

    //创建Fragment时执行onCreateView()方法
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        //获得Fragment需要使用的layout.xml布局文件
        View ret=inflater.inflate(R.layout.fragment_news_detail,container,false);

        //返回创建Fragment实例时提供的参数(如果有的话).
        newsTitleTxt= (TextView) ret.findViewById(R.id.newsTitleTxt);
        newsDetailTxt= (TextView) ret.findViewById(R.id.newsDetailTxt);

        Bundle arguments=getArguments();
        if(arguments!=null){
            long id=arguments.getLong("id");
            newsTitleTxt.setText("新闻标题"+((id<10)?"0"+id:id));
            newsDetailTxt.setText("此处显示新闻内容"+((id<10)?"0"+id:id)
                    +"\n为简单，”新闻“内容是固定的。可改为基于文件系统或数据库的真新闻。");
        }
        return ret;
    }
    public void setNews(int id){
        newsTitleTxt.setText("新闻标题"+((id<10)?"0"+id:id));
        newsDetailTxt.setText("此处显示新闻内容"+((id<10)?"0"+id:id)
                +"\n为简单起见，”新闻“内容是固定的。请考虑改为基于文件系统或数据库的真新闻。"
                +"\n这是在平板中的显示，是否值得庆祝一下？");
    }
}