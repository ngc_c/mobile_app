package com.example.shaoheng.tenthweek;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class TextBookActivity extends AppCompatActivity {

    //要操作的文件名
    final String FILE_NAME="memo.txt";
    //标识文件是否被修改，初值为false
    boolean ischanged=false;
    //定义组件
    EditText editFileEdt,fielPathEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_text_book);

        //初始化组件
        editFileEdt= (EditText) findViewById(R.id.editFileEdt);
        fielPathEdt= (EditText) findViewById(R.id.fielPathEdt);

        //设置编辑框中内容发生变化时的事件监听器，以检测、记录内容是否发生变化
        editFileEdt.addTextChangedListener(textChangedListener);
    }

    //重写创建菜单的方法
    @Override
   /* public boolean onCreateOptionsMenu(Menu menu){
        super.onCreateOptionsMenu(menu);
        //添加4个菜单项，分成2组
        int group1=1;
        int group2=2;

        menu.add(group1,201,1,"打开文件");
        menu.add(group1,202,2,"保存文件");
        menu.add(group2,203,3,"退出应用");
        menu.add(group2,204,4,"关于...");
        //从Android4.5开始，不支持为菜单增加图标，可以通过重写onMenuOpened()实现
        // 如果希望显示菜单，就要返回true
        return true;
        /*菜单Menu和子菜单SubMenu对象的add方法参数(还有其他形式)
        add(intgroupId,intitemId,intorder,CharSequencetitle)//向菜单添加一个新项目
        addSubMenu(intgroupId,intitemId,intorder,CharSequencetitle)//在菜单中添加新子菜单
        groupId int:菜单项的分组标识，使用Menu.NONE代表不属于任何组
        itemId int:菜单项的唯一标识，若不需要唯一标识，则使用Menu.NONE
        order int:菜单项的排列顺序，无所谓顺序时，使用Menu.NONE
        title CharSequence:菜单项的显示文本

    }*/


    public boolean onCreateOptionsMenu(Menu menu){
        //一个menu可以包括多个子菜单
        SubMenu subMenu=menu.addSubMenu(0,Menu.NONE,Menu.NONE,"文件操作");
        //子菜单可以包括多个菜单项
        MenuItem openItem=subMenu.add(2,201,1,"打开文件");
        openItem.setIcon(R.drawable.open);

        MenuItem saveItem=subMenu.add(2,202,2,"保存文件");
        saveItem.setIcon(R.drawable.save);

        menu.add(2,203,3,"退出应用");
        menu.add(2,204,4,"关于...");

        //显示菜单请返回true
        return true;
    }

    //定义事件监听器,处理各菜单项（子菜单项）被选中时的业务
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        switch(item.getItemId()){
            case 201: //读文件
                readFile();
                break;
            case 202: //保存文件后，重置文件内容被修改标志
                writeFile();
                ischanged=false;
                break;
            case 203: //退出处理：若有改动需确认是否保存
                if(ischanged){ //当有改动时
                    AlertDialog.Builder alterDialog=new AlertDialog.Builder(TextBookActivity.this);
                    alterDialog.setTitle("提示")
                            .setMessage("文件有改动，是否保存")
                            .setPositiveButton("保存",new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialogInterface,int i){
                                    //当确定保存时，写文件后退出
                                    writeFile();
                                    finish();
                                }
                    })
                            .setNegativeButton("不保存",new DialogInterface.OnClickListener(){
                                @Override
                                public void onClick(DialogInterface dialogInterface,int i){
                                    //选择不保存时，直接退出
                                    finish();
                                }
                            })
                            .create()
                            .show();
                }else{//没有改动时直接退出
                    finish();
                    Toast.makeText(TextBookActivity.this,"安全退出",
                            Toast.LENGTH_SHORT).show();
                }
                break;
            case 204:
                AlertDialog.Builder alterDialog=new AlertDialog.Builder(TextBookActivity.this);
                alterDialog.setTitle("提示")
                        .setMessage("通过这个Demo，学会各种菜单。")
                        .setPositiveButton("确认",new DialogInterface.OnClickListener(){
                            @Override
                            public void onClick(DialogInterface dialogInterface,int i){

                            }
            })
                        .create()
                        .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    //写入文件
    private void writeFile(){
        //取得文件内容（适用于小文件）
        String str=editFileEdt.getText().toString();
        try{
            //定义输出流对象并打开文件
            FileOutputStream fos=openFileOutput(FILE_NAME, Context.MODE_PRIVATE);
            //用write方法写入字节流
            fos.write(str.getBytes());
            //关闭文件
            fos.close();
        }catch (FileNotFoundException e){ //异常捕获（由IDE自动生成）
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    //读文件内容
    private void readFile(){
        try{
            //定义文件输入流对象并打开文件
            FileInputStream fis=openFileInput(FILE_NAME);
            //定义用于读入文件的缓冲区
            byte[]buff=new byte[1024];
            //已读入字节数
            int hasRead=0;
            //定义并初始字符串构造器对象
            StringBuilder sb=new StringBuilder();
            //循环将文件内容读入缓冲区并合并到字符串构造器
            while((hasRead=fis.read(buff))>0){
                sb.append(new String(buff,0,hasRead));
            }
            //关闭文件
            fis.close();
            //更新组件内容
            editFileEdt.setText(sb);
            //更新文件路径信息
            fielPathEdt.setText(getFilesDir().toString());
        }catch(FileNotFoundException e){ //异常捕获（由IDE自动生成）
            e.printStackTrace();
        }catch (IOException e){
            e.printStackTrace();
        }
    }
    //定义编辑框中文本改变的监听器（框架由IDE辅助生成）
    private TextWatcher textChangedListener=new TextWatcher(){
        @Override
        public void beforeTextChanged(CharSequence charSequence,int i,int i1,int i2){
        }
        @Override
        public void onTextChanged(CharSequence charSequence,int i,int i1,int i2){
        }
        //当文本改变后，将已修改标记改为true
        @Override
        public void afterTextChanged(Editable editable){
            ischanged=true;
        }
    };
}
