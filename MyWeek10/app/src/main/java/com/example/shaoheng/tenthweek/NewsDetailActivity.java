package com.example.shaoheng.tenthweek;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

public class NewsDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news_detail);

        //取出Intent中附加的Bundle
        Bundle bundle=getIntent().getExtras();
        if(bundle!=null){
            //创建Fragment对象
            NewsDetailFragment detailFragment=new NewsDetailFragment();
            //设置传递到framgment中的参数
            detailFragment.setArguments(bundle);
            //获取所在fragment的父容器的管理器
            FragmentManager manager=getSupportFragmentManager();
            //获得Fragment事务对象
            FragmentTransaction transaction=manager.beginTransaction();
            //把detail_fragment_container替换成新detailFragment
            transaction.replace(R.id.detail_fragment_container,detailFragment);
            //提交事务
            transaction.commit();
            //在事务提交后，会进到framment的创建生命周期
        }
    }
}
