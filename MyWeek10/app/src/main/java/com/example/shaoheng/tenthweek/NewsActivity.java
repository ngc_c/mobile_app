package com.example.shaoheng.tenthweek;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

public class NewsActivity extends AppCompatActivity implements
NewsListFragment.OnNewsSelectedListener{
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        /*在这里，系统会根据运行的机型，决定选用哪一个activity_news.xml
        平板中用layout-large/activity_news.xml,手机中用activity_news.xml
        */
    }
    /*@Override
    public void onPointerCaptureChanged(boolean hasCapture){
        super.onPointerCaptureChanged(hasCapture);
    }*/

    @Override
    public void onNewsSelected(Bundle bundle){
        //获取所在fragment的父容器的管理器
        FragmentManager manager=getSupportFragmentManager();
        //获得要操作的fragment中的组件
        // 若在平板中运行，匹配的layout是large/activity_news.xml,R.id.fragment_detail存在，返回非null
        // 若在手机中运行，匹配的layout是activity_news.xml,R.id.fragment_detail存在，返回非nul
        Fragment detailFragment=manager.findFragmentById(R.id.fragment_detail);

        //根据detailFragment是否为null，判断是在平板上还是手机上运行
        if(detailFragment!=null){ //在平板上运行时执行NewsDetailFragment类的setNews()方法显示
            NewsDetailFragment newsDetailFragment=(NewsDetailFragment)detailFragment;
            newsDetailFragment.setNews(bundle.getInt("position",0));
        }else{   //在手机上运行时，打开新的Activity
            Intent intent=new Intent(this,NewsDetailActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }
}

