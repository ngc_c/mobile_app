package com.example.msi_pc.myweek4;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.ToggleButton;

public class MainActivity extends Activity {
    //定义显示结果的文本框
    private TextView textReslut;

    //定义单选按钮组
    private RadioGroup radioGroupId;
    //定义复选框按钮
    private CheckBox checkBoxKexing, checkBoxGuoyao, checkBoxKangxinuo;
    //定义开关组件
    private Switch switchMonitorCAll, switchRoomLeaderCall;
    //定义ToggleButton组件
    private ToggleButton toggleButtonWideScreen;

    //定义第1部分的布局对象
    private LinearLayout linearLayoutIDVacccine;
    //定义按钮对象
    private Button buttonShow, buttonClear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //初始化显示结果的文本框组件
        textReslut = (TextView) findViewById(R.id.textResult);
        //初始化按钮组对象并设置事件监听
        radioGroupId = (RadioGroup) findViewById(R.id.radioGroupId);
        radioGroupId.setOnCheckedChangeListener(radioGroupChangeListener);
        //初始化复选框按钮
        checkBoxKexing = (CheckBox) findViewById(R.id.checkBoxKexing);
        checkBoxGuoyao = (CheckBox) findViewById(R.id.checkBoxGuoyao);
        checkBoxKangxinuo = (CheckBox) findViewById(R.id.checkBoxKangxinuo);

        //设置复选框按钮的事件监听
        checkBoxKexing.setOnCheckedChangeListener(checkBoxChangeListener);
        checkBoxGuoyao.setOnCheckedChangeListener(checkBoxChangeListener);
        checkBoxKangxinuo.setOnCheckedChangeListener(checkBoxChangeListener);
        //初始化开关组件
        switchMonitorCAll = (Switch) findViewById(R.id.switchMonitorCall);
        switchRoomLeaderCall = (Switch) findViewById(R.id.switchRoomLeaderCall);

        //注册开关组件的事件监听
        switchMonitorCAll.setOnCheckedChangeListener(switchChangeListener);
        switchRoomLeaderCall.setOnCheckedChangeListener(switchChangeListener);
        //初始化toggleButton组件
        toggleButtonWideScreen = (ToggleButton) findViewById(R.id.toggleButtonWideScreen);

        //注册toggleButton组件的事件监听
        toggleButtonWideScreen.setOnCheckedChangeListener(switchChangeListener);

        //初始化要操作的第一部分的布局对象
        linearLayoutIDVacccine = (LinearLayout) findViewById(R.id.linearLayoutIDVaccine);
        //初始化按钮对象
        buttonShow = (Button) findViewById(R.id.buttonShow);
        buttonClear = (Button) findViewById(R.id.buttonClear);

        //注册按钮组件的事件监听
        buttonShow.setOnClickListener(buttonOnClickListener);
        buttonClear.setOnClickListener(buttonOnClickListener);

    }
    //定义单选按钮改变值的监听事件
    private RadioGroup.OnCheckedChangeListener radioGroupChangeListener=new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup radioGroup, int i) {

            //用getCheckedRadioButtonId()获得按钮组中改变的按钮
            int id = radioGroup.getCheckedRadioButtonId();
            //按下不同的按钮，在信息区显示不同的信息
            switch(id){case R.id.radioTeacher:
                textReslut.setText(R.string.teacher_hint);
                break;
                case R.id.radioStudent:
                    textReslut.setText(R.string.student_hint);
                    break;
                default:
                    textReslut.setText(R.string.no_hint);
                    break;
            }
        }
    };
    //定义复选框的监听事件对象
    private CompoundButton.OnCheckedChangeListener checkBoxChangeListener
            = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            //获得改变后的复选框
            int id = compoundButton.getId();
            switch (id) {
                case R.id.checkBoxKexing:
                    if (b) {
                        textReslut.setText("你接种了科兴疫苗...");
                    } else {
                        textReslut.setText("你没有接种科兴疫苗...");
                    }
                    break;
                case R.id.checkBoxGuoyao:
                    if (b) {
                        textReslut.setText("你接种了国药疫苗...");
                    } else {
                        textReslut.setText("你没有接种国药疫苗...");
                    }
                    break;
                case R.id.checkBoxKangxinuo:
                    if (b) {
                        textReslut.setText("你接种了康希诺疫苗...");
                    } else {
                        textReslut.setText("你没有接种康希诺疫苗...");
                    }
                    break;
            }
        }
    };
    //定义开关组件的监听事件对象
    private CompoundButton.OnCheckedChangeListener switchChangeListener
            = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton compoundButton, boolean b) {

            //获得改变后的开关组件
            int id = compoundButton.getId();
            switch (id) {
                case R.id.switchMonitorCall:
                    if (b) {
                        textReslut.setText("忘记了上网课时间让班长叫");
                    } else {
                        textReslut.setText("忘记了上网课时间不让班长叫");
                    }
                    break;
                case R.id.switchRoomLeaderCall:
                    if (b) {
                        textReslut.setText("忘记了上网课时间让舍长叫");
                    } else {
                        textReslut.setText("忘记了上网课时间不让舍长叫");
                    }
                    break;
                case R.id.toggleButtonWideScreen:
                    if (b) {
                        textReslut.setText("窄屏设置");
                        linearLayoutIDVacccine.setOrientation(LinearLayout.VERTICAL);
                        linearLayoutIDVacccine.setShowDividers(LinearLayout.SHOW_DIVIDER_MIDDLE);
                    } else {
                        textReslut.setText("宽屏设置");
                        linearLayoutIDVacccine.setOrientation(LinearLayout.HORIZONTAL);
                        linearLayoutIDVacccine.setShowDividers(LinearLayout.SHOW_DIVIDER_NONE);
                    }
                    break;
            }
        }
    };
    //响应按钮事件的代码
    private View.OnClickListener buttonOnClickListener
            =new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            switch (id){
                case R.id.buttonShow:
                    String str = new String("您目前的选择是：\n");

                    str += "1. 身份：";
                    switch(radioGroupId.getCheckedRadioButtonId()){
                        case R.id.radioTeacher:
                            str += "老师\n";
                            break;
                        case R.id.radioStudent:
                            str += "学生\n";
                            break;
                        case R.id.radioOthers:
                            str += "其他\n";
                            break;
                        default:
                            str += "(未选择)\n";
                            break;
                    }

                    str += "2. 您打过的疫苗：";
                    String strTemp = new String("");
                    if(checkBoxGuoyao.isChecked())
                        strTemp += "国药 ";
                    if(checkBoxKexing.isChecked())
                        strTemp += "科兴 ";
                    if(checkBoxKangxinuo.isChecked())
                        strTemp += "康希诺 ";
                    if(strTemp.isEmpty())
                        strTemp += "(未接清单内疫苗)";
                    str += (strTemp + "\n");

                    str = str + "3. 忘上网课谁叫你：";
                    strTemp = new String("");
                    if(switchMonitorCAll.isChecked())
                        strTemp += "班长 ";
                    if(switchRoomLeaderCall.isChecked())
                        strTemp += "舍长 ";
                    if(strTemp.isEmpty())
                        strTemp += "(自律好青年)";
                    str += (strTemp + "\n");

                    str = str + "现在的显示模式是：";
                    if(toggleButtonWideScreen.isChecked())
                        str += "窄屏 ";
                    else
                        str += "宽屏 ";

                    textReslut.setText(str);
                    textReslut.setTextColor(getResources().getColor(R.color.text_color));
                    textReslut.setBackground(getResources().getDrawable(R.drawable.ytu_512));

                    break;
                case R.id.buttonClear:
                    radioGroupId.check(-1);
                    checkBoxGuoyao.setChecked(false);
                    checkBoxKexing.setChecked(false);
                    checkBoxKangxinuo.setChecked(false);
                    switchMonitorCAll.setChecked(false);
                    switchRoomLeaderCall.setChecked(false);
                    toggleButtonWideScreen.setChecked(false);
                    textReslut.setText("现在还没有选择！");
                    break;
            }
        }
    };
}
