# 第 4 周 《手机移动开发》实践报告 
### 实践目的 
1. 了解View类族的常用属性，并能够通过文档找到关于属性、方法的说明和用法； 
1. 能够用布局嵌套构造实用的用户界面； 
1. 能够初步将文本、颜色、尺寸及图像资源用在项目开发中，提高程序质量；
1. 能够将CompoundButton中的组件用于应用程序开发
### 实践内容 1：复合按钮 
#### 模块名 
activity_main.xml 
#### 完成的主要功能 
1. 选择身份
1. 选择疫苗种类
1. 选择忘记上网课谁提醒
1. 适应窄屏的页面布置
1. 显示全部选择内容
1. 清除选择
#### 完成过程中遇到的问题及解决办法 
1. 最开始从老师的代码中复制发现有一些问题后及时手打修改
1. activity_main中代码多了vertical问同学后修正 
#### 完成该实践项目的收获及感想 
1. 任何问题都可以解决，需要我们上心任何事情都可以找到解决的办法
1. 要跟老师同学们虚心请教
### 实践内容 2：资源文件的应用 
#### 模块名 
Mainactivity 
#### 完成的主要功能 
1. 在res/values/srings.xml中指定字符串并应用
1. 在res/values/colors.xml中指定颜色并应用
1. 使用图片资源
1. 应用样式和主题资源 
#### 完成过程中遇到的问题及解决办法 
1. 监听项目没有设置完全，也是询问同学后解决
#### 完成该实践项目的收获及感想 
1. 学会了消除魔法字符的重要功能
1. 要更加的细致细心
1. 积极听课争取当堂完成代码问题