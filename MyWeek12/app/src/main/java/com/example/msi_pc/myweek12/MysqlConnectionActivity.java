package com.example.msi_pc.myweek12;

import android.os.Bundle;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.io.Reader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Handler;

public class MysqlConnectionActivity extends AppCompatActivity {
    //定义组件
    private EditText readerNumEdt, readerNameEdt, readerPhoneEdt;
    private TextView resultTxt;
    private RadioGroup readerTypeRadioGroup;
    //定义读者对象
    Reader reader = null;
    //操作远程数据库是耗时操作，需要建立新线程操作，其结果通过 Handle 影响主 UI 线程
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 0x11:
                    String s = (String) msg.obj;
                    resultTxt.setText(s);
                    break;
                case 0x12:
                    String ss = (String) msg.obj;
                    resultTxt.setText(ss);
                    break;
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mysql_connection);
//初始化组件
        readerNumEdt = (EditText)findViewById(R.id.readerNumEdt);
        readerNameEdt = (EditText)findViewById(R.id.readerNameEdt);
        readerPhoneEdt = (EditText)findViewById(R.id.readerPhoneEdt);
        resultTxt = (TextView)findViewById(R.id.resulteTxt);
        readerTypeRadioGroup = (RadioGroup)findViewById(R.id.readerTypeRadioGroup);
//为数据库的增删改查按钮注册事件监听器
//findViewById(R.id.addReaderBnt).setOnClickListener(OnClickListener);
//findViewById(R.id.deleteReaderBnt).setOnClickListener(OnClickListener);
//findViewById(R.id.updateReaderBnt).setOnClickListener(OnClickListener);
        findViewById(R.id.queryReaderBnt).setOnClickListener(OnClickListener);
    }
    //定义事件监听器
    private View.OnClickListener OnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Message message = handler.obtainMessage();
            message.what = 0x11;
//设置时间格式
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd " + "HH:mm:ss");
//分情况处理
            switch (view.getId()) {
                case R.id.addReaderBnt://增加读者
                    break;
                case R.id.deleteReaderBnt: //删除
                    break;
                case R.id.updateReaderBnt:
                    break;
                case R.id.queryReaderBnt: //按编号查询
// 创建一个线程来连接数据库并获取数据库中对应表的数据
                    new Thread(new Runnable() {
                        public void run() {
// 调用数据库工具类 DBUtils 的 getInfoByName 方法获取数据库表中数据
                            ResultSet res = DBUtils.queryReader(readerNumEdt.getText().toString());
//显示结果
                            String str = "";
                            int i = 1;
                            try {
                                while (res.next()) {
//构造和装配一个 Reader 对象
                                    Reader reader = new Reader();
                                    reader.setReader_number(res.getString("reader_number"));
                                    reader.setReader_name(res.getString("reader_name"));
                                    reader.setReader_type(res.getString("reader_type"));
                                    reader.setReader_phone(res.getString("reader_phone"));
                                    reader.setReader_password(res.getString("reader_password"));
                                    reader.setReader_createtime(
                                            String.valueOf(res.getDate("reader_createtime")));
//将构造的对象转为字符串合并到结果中
                                    str = str + i + "." + reader.toString();
                                    i++;
                                }
                            } catch (SQLException throwables) {
                                throwables.printStackTrace();
                            }
//设置显示信息
                            if (str != "") {
                                message.what = 0x12;
                                message.obj = "查询结果:\n" + str;
                            } else {
                                message.obj = "查询结果为空";
                            }
// 发消息通知主线程更新 UI
                            handler.sendMessage(message);
                        }
                    }).start();
                    break;
            }
        }
    };
}