package com.example.msi_pc.myweek12;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;

public class SocketClientActivity extends AppCompatActivity {
    private TextView chatmessage=null;
    private EditText sendmessage=null;
    private static final String HOST="10.21.255.236";
    private static final int PORT =29898;
    private Socket socket=null;

    private BufferedReader bufferedReader=null;
    private PrintWriter printWriter=null;
    private String msg="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socket_client);
        //初始化组件
        chatmessage= (TextView) findViewById(R.id.chatmessage);
        sendmessage= (EditText) findViewById(R.id.sendmessage);

        //创建线程，用于建立和服务器的连接，获得套接字对象
        //建立连接后，构造接受输入流的bufferedReader对象和用于输出的printWrite对象
        //由于建立和“远程”服务器的连接是一个无法保证即时完成的操作，故需要在线程中完成
        new Thread(){
            //线程运行的代码
            public void run(){
                try{
                    //指定IP和端口号创建套接字
                    socket=new Socket(HOST,PORT);
                    //使用套接字的输入流构造BufferedReader对象
                    bufferedReader=new BufferedReader(
                            new InputStreamReader(socket.getInputStream())
                    );
                    printWriter=new PrintWriter(
                            new BufferedWriter(
                                    new OutputStreamWriter(socket.getOutputStream())
                            ),true
                    );
                } catch (Exception e) {
                    e.printStackTrace();
                }
                super.run();
            }
        }.start();//启动线程

        //设置“发送”按钮的事件监听，当点击按钮时，向服务器发送信息
        findViewById(R.id.sendbutton).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //获取输入框的内容
                String message=sendmessage.getText().toString();
                Log.d("TAG","将发送1: "+message);
                //若套接字同服务器的链接存在且输出流也存在，则发送消息
                if (socket!=null&&printWriter!=null){
                    if (socket.isConnected()){
                        if (!socket.isOutputShutdown()){
                            //将输入框的内容发送到服务器
                            printWriter.println(message);
                            //刷新缓冲区,即将缓冲区中的数据立刻发送出云,同时清空缓冲区
                            printWriter.flush();
                            //设置chatmessage的内容
                            chatmessage.setText(chatmessage.getText().toString()+"\n"+"发送: "+message);
                            //清空sendmessage的内容，以便下次输入
                            sendmessage.setText("");
                        }
                    }
                }
            }
        });

        //创建线程，接收从服务器信息
        new Thread(){
            //线程运行的代码
            public void run(){
                while(true){
                    //若套接字同服务器的链接存在且输入流也存在，则接收消息并显示聊天信息
                    if(socket!=null&&bufferedReader!=null){
                        if (socket.isConnected()){
                            if (!socket.isInputShutdown()){
                                try{
                                    //输入缓冲区非空时，接收信息
                                    if ((msg=bufferedReader.readLine())!=null){
                                        Log.i("TAG",msg);
                                        chatmessage.setText(chatmessage.getText().toString()+"\n"+"接收: "+msg);
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();//显示异常信息
                                }
                            }
                        }
                    }
                }
            }
        }.start();
    }
}
