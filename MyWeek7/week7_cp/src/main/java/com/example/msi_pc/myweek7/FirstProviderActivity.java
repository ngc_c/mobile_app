package com.example.msi_pc.myweek7;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class FirstProviderActivity extends AppCompatActivity {
    //定义要操作 ContentProvider 对象的解析器对象
    ContentResolver contentResolver;
    //定义要操作内容提供者的地址
    Uri uri = Uri.parse("content://com.he.week7_cp.firstprovider");
    //定义按钮组件
    Button insert, delete, update, query;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first_provider);
        //通过 getContentResolver()获取系统操作用的解析器 contentResolver
        contentResolver = getContentResolver();

        //初始化组件并设置事件监听器
        insert = (Button) findViewById(R.id.insert);
        delete = (Button) findViewById(R.id.delete);
        update = (Button) findViewById(R.id.update);
        query = (Button) findViewById(R.id.query);
        insert.setOnClickListener(onClickListener);
        delete.setOnClickListener(onClickListener);
        update.setOnClickListener(onClickListener);
        query.setOnClickListener(onClickListener);

    }

    //定义事件监听器
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.insert: //增加
                    insert(view);
                    break;
                case R.id.delete:
                    delete(view);
                    break;
                case R.id.update:
                    update(view);
                    break;
                case R.id.query:
                    query(view);
                    break;
            }
        }
    };
    //实现插入数据
    public void insert(View source){
        //把要插入的值封装到 ContentValue 对象中
        ContentValues values=new ContentValues();
        values.put("name","zhangsan");
        values.put("age",25);
        //调用 ContentResolver 的 insert()方法完成插入
        //返回的是该 Uri 对应的 ContentProvider 的 insert()的返回值
        Uri newUri = contentResolver.insert(uri, values);
        //以下是完成插入操作后的逻辑
        Toast.makeText(this, "远程 ContentProvide 新插入记录的 Uri 为：" + newUri, Toast.LENGTH_LONG).show();


    }
    //实现删除数据
    public void delete(View source){
        int count=contentResolver.delete(uri,"age>120",null);
        Toast.makeText(this, "远程 ContentProvide 删除记录数为：" + count, Toast.LENGTH_LONG).show();
    }
    //更新
    public void update(View source){
        ContentValues values=new ContentValues();
        values.put("name","zhangsan");
        int count=contentResolver.update(uri,values,"name=?",new String[]{"zhangsan"});
        Toast.makeText(this, "远程 ContentProvide 更新记录数为：" + count, Toast.LENGTH_LONG).show();
    }
    //查询
    public void query(View source) {
        Cursor cursor = contentResolver.query(uri,new String[]{"name","age"},"name LIKE? and age>21",new String[]{"zhang"},"age");
        Toast.makeText(this, "远程 ContentProvide 返回的 Cursor 为：" + cursor, Toast.LENGTH_LONG).show();
    }
}
