package com.example.msi_pc.myweek11;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

public class MyService extends Service {
    public MyService() {
    }

    @Override
    public void onCreate(){
        super.onCreate();
        Log.d("Myservice","onCreateexecuted!");
    }

    //服务进入活动期时执行
    @Override
    public int onStartCommand(Intent intent,int flags,int startId){
        String msg=intent.getStringExtra("message");
        Log.d("Myservice","onStartCommandexecuted!"+
                "intent:"+msg+
                ",flags:"+flags+
                ",startId:"+startId);
        try {
            Thread.sleep(20000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return super.onStartCommand(intent,flags,startId);
    }

    //当服务被销毁时执行
    @Override
    public void onDestroy(){
        Log.d("Myservice","onDestroyexecuted!");
        super.onDestroy();
    }



    public class MyBinder extends Binder {
        public MyService getService(){
            return MyService.this;//返回Service的当前实例（就可访问Service中包含的数据和方法了）
        }
    }

    private MyBinder myBinder=new MyBinder();
    private String valueToShow="我代表希望传出去的值";
    public String getValue(){return valueToShow;}

    @Nullable
    @Override
    public IBinder onBind(Intent intent){
        String msg=intent.getStringExtra("message");
        Log.d("MyService","绑定时收到MainActivityintent消息:"+msg);
        return myBinder;
    }

    //解绑时执行
    @Override
    public boolean onUnbind(Intent intent){
        Log.d("MyService","散伙就散伙——onUnbind executed!");
        return super.onUnbind(intent);
    }

    //这个自定义函数代表所有的在后台默默搬砖完成任务的函数
    public String doSomeOperation(String str){
        Log.d("MyService","收到MainActivity要求:"+str);
        return"谁怕谁，跟一个!";
    }
}

