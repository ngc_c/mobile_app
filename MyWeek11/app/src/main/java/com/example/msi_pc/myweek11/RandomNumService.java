package com.example.msi_pc.myweek11;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomNumService extends Service {
    public RandomNumService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new MyBinder();
    }
    public class MyBinder extends Binder{
        public RandomNumService getService(){
            return RandomNumService.this;
        }
    }

    //创建获取随机号码的方法
    public List getRandomNumber(){
        //创建ArrayList数组
        List resArr=new ArrayList();
        String strNumber="";
        //将随机获取的数字转换为字符串添加到ArrayList数组中
        for(int i=0;i<7;i++){
            int number=new Random().nextInt(33)+1;
            //把生成的随机数格式化为两位的字符串
            if(number<10){ //在数字1~9前加0
                strNumber="0"+String.valueOf(number);
            }else{
                strNumber=String.valueOf(number);
            }
            resArr.add(strNumber);
        }
        return resArr;//将数组返回
    }
    @Override
    public void onDestroy(){   //销毁该Service
        super.onDestroy();
    }
}