package com.example.msi_pc.myweek11;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;

public class MusicService extends Service {
    public MusicService() {
    }

    static boolean isplay;//定义当前播放状态
    MediaPlayer player;   //MediaPlayer对象

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    //在创建服务时，创建MediaPlayer对象并加载播放的音乐文件
    @Override
    public int onStartCommand(Intent intent,int flags,int startId){
        //如果没有播放音乐
        if(!player.isPlaying()){
            //播放音乐
            player.start();
            //设置当前状态正在播放音乐
            isplay=player.isPlaying();
        }
        return super.onStartCommand(intent,flags,startId);
    }
    //释放资源
    @Override
    public void onDestroy(){
        //停止音频的播放
        player.stop();
        //设置当前状态为没有播放音乐
        isplay=player.isPlaying();
        //释放资源
        player.release();
        super.onDestroy();

    }
}
