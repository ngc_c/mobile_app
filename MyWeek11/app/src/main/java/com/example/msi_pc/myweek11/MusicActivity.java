package com.example.msi_pc.myweek11;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

public class MusicActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music);
        //设置全屏显示
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        ImageButton playBtn= (ImageButton) findViewById(R.id.playBtn);  //获取“播放/停止”按钮
        //启动服务与停止服务，实现播放背景音乐与停止播放背景音乐
        playBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                //判断音乐播放的状态
                if(MusicService.isplay==false){
                    //启动服务，从而实现播放背景音乐
                    startService(new Intent(MusicActivity.this,MusicService.class));
                    //更换播放背景音乐图标
                    ((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.play,null));
                }else{
                    //停止服务，从而实现停止播放背景音乐
                    stopService(new Intent(MusicActivity.this,MusicService.class));
                    //更换停止背景音乐图标
                    ((ImageButton)v).setImageDrawable(getResources().getDrawable(R.drawable.stop,null));
                }
            }
        });
    }
    //实现进入界面时，启动背景音乐服务
    @Override
    protected void onStart(){
        //启动服务，从而实现进入应用时就播放背景音乐
        startService(new Intent(MusicActivity.this,MusicService.class));
        super.onStart();
    }
}