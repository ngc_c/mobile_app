package com.example.msi_pc.myweek11;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // 1. 理解Service：启动和停止Service的事件监听
        findViewById(R.id.start_service).setOnClickListener(onClick);
        findViewById(R.id.stop_service).setOnClickListener(onClick);

        //2. Activity和Service间的通信：绑定、解绑、操作Service的事件监听
        findViewById(R.id.bind_service).setOnClickListener(onClick);
        findViewById(R.id.unbind_service).setOnClickListener(onClick);
        findViewById(R.id.operate_service).setOnClickListener(onClick);

        //3. 耗时服务解决方案
        findViewById(R.id.new_Thread).setOnClickListener(onClick);
        findViewById(R.id.intent_service).setOnClickListener(onClick);

        //4. 系统服务用法：以通知服务为例
        findViewById(R.id.notify_service_fore).setOnClickListener(onClick);

        //5. 服务的应用
        findViewById(R.id.music_service).setOnClickListener(onClick);
        findViewById(R.id.randomNUm_sevice).setOnClickListener(onClick);
    }
    //定义事件监听的逻辑
    private View.OnClickListener onClick = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            switch (view.getId()){
                // 1. 下面是用于体验启动和停止服务的分支
                case R.id.start_service:  //启动Service
                    intent.setClass(MainActivity.this,MyService.class);
                    intent.putExtra("message","启动服务");
                    startService(intent);
                    break;
                case R.id.stop_service:   //停止Service
                    intent.setClass(MainActivity.this,MyService.class);
                    stopService(intent);
                    break;
                //2. 下面是用于体验绑定、解绑的分支
                case R.id.bind_service:  //绑定Service
                    intent.setClass(MainActivity.this,MyService.class);
                    Log.d("MainActivity","要绑定Service，请Intent带句话——");
                    intent.putExtra("message","耍个朋友噻！");
                    bindService(intent,myServiceConn,Context.BIND_AUTO_CREATE);
                    break;
                case R.id.operate_service:  //操作Serivce
                    if(myService==null)
                        return;
                    //调用服务中用于处理业务逻辑的代码
                    Log.d("MainActivity","向Service发起请求，一起干点啥");
                    //用绑定后获得的服务实例，调用服务完成一些操作
                    String returnValue=myService.doSomeOperation("走一个！");
                    Log.d("MainActivity","接收Service返回的结果："+returnValue);
                    break;
                case R.id.unbind_service:    //解绑Service
                    unbindService(myServiceConn);
                    break;
                //3. 下面体验在Service中执行耗时任务的方法
                case R.id.new_Thread:   //在Service中开新线程执行耗时任务的方法
                    intent.setClass(MainActivity.this,MyNewThreadService.class);
                    startService(intent);
                    break;
                case R.id.intent_service:   //利用IntentService执行耗时任务的方法
                    intent.setClass(MainActivity.this,MyIntentService.class);
                    startService(intent);
                    break;
                //4. 系统Service应用
                case R.id.notify_service_fore:   //通知服务
                    show_notification();
                    break;
                //5. Service应用
                case R.id.music_service:  //后台音乐
                    intent.setClass(MainActivity.this,MusicActivity.class);
                    startActivity(intent);
                    break;
                case R.id.randomNUm_sevice: //随机号码
                    intent.setClass(MainActivity.this,RandomNumActivity.class);
                    startActivity(intent);
                    break;
            }
        }
    };

    private MyService myService;
    private ServiceConnection myServiceConn=new ServiceConnection(){
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder){
            Log.d("ServiceConnection","绑定"+componentName.getShortClassName());
            myService=((MyService.MyBinder)iBinder).getService();
            Log.d("ServiceConnection","绑定后就能从服务中取得值——"+myService.getValue());
        }
        @Override
        public void onServiceDisconnected(ComponentName componentName){
            Log.d("ServiceConnection","要与"+componentName.getShortClassName()+"解绑");
            myService=null;
        }
    };

    private void show_notification(){
        //用intent表现出我们要启动Notification的意图
        Intent intent=new Intent(android.provider.Settings.ACTION_SETTINGS);
        //PendingIntent对象描述要执行的Intent和目标操作,getActivity()方法有多种调用形式
        PendingIntent pendingIntent= PendingIntent.getActivity(this,
                1,intent,PendingIntent.FLAG_UPDATE_CURRENT);
        //建立一个通知管理类的对象，负责发通知、清除通知等
        NotificationManager manager=(NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        /*
        if(Build.VERSION.SDK_INT>=26)  //当sdk版本大于26
        {
            String id="112";    //构建渠道需要唯一指定的ID
            CharSequence description="用于演示通知的Demo";//指定用户在系统设置中看到的描述
            NotificationChannel channel=new NotificationChannel(id,description,
                    NotificationManager.IMPORTANCE_LOW);
            //channel.enableLights(true);   //开启闪光（如果android设备支持的话
            //channel.setLightColor(Color.WHITE);
            // channel.enableVibration(true);// 开启振动（如果android设备支持的话）
            // channel.setVibrationPattern(newlong[]{100,200,100,200});//振动设置
            // channel.setBypassDnd(true);//设置绕过免打扰模式
            // channel.canBypassDnd();//检测是否绕过免打扰模式
            // /channel.setLockscreenVisibility(Notification.VISIBILITY_SECRET);//在锁屏界面上显示这条通知
            // 为管理类对象指定创建的渠道
            manager.createNotificationChannel(channel);
            //创建通知对象
            Notification notification=new Notification.Builder(MainActivity.this,id)
                    //.setCategory(Notification.CATEGORY_MESSAGE)
                    .setSmallIcon(R.mipmap.ic_launcher) //设置通知出现在手机顶部的小图标
                    .setContentTitle("通知标题：学而")//设置通知栏中的标题
                    .setContentText(getString(R.string.notification1))//设置通知栏中的内容
                    .setContentIntent(pendingIntent)//将PendingIntent对象传入该方法中
                    .setWhen(System.currentTimeMillis())//设置通知出现的时间（响应后立马出现通知）
                    //.setAutoCancel(true)//点击通知后，通知自动消失
                    .build();
            //显示通知，第一个参数为id，每个通知的id都必须不同。第二个参数为具体的通知对象
            manager.notify(1,notification);
        }
        else{
            //当sdk版本小于26
            Notification notification=new NotificationCompat.Builder(this)
                    .setContentTitle("通知标题：举一反三")
                    .setContentText(getString(R.string.notification2))
                    .setWhen(System.currentTimeMillis())
                    .setSmallIcon(R.mipmap.ic_launcher)
                    //.setLargeIcon(BitmapFactory.decodeResource(getResources(),R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    //.setSound(Uri.fromFile(newFile("/system/media/audio/ringtones/Luna.ogg")))//.setVibrate(newlong[]{0,1000,1000,1000}
                    //.setLights(Color.GREEN,1000,1000)
                    //.setPriority(NotificationCompat.PRIORITY_MAX)
                    .build();
            //用于显示通知，第一个参数为id，每个通知的id都不同。第二个参数为具体的通知对象
            manager.notify(1,notification);
        }
        */
    }
}

