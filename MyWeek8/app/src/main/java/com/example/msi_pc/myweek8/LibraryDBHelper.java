package com.example.msi_pc.myweek8;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by MSI-PC on 2022/4/18.
 */

public class LibraryDBHelper extends SQLiteOpenHelper {
    //数字图书馆数据库版本号
    public static final int DATABASE_VERSION=1;
    //数字图书馆数据库名
    private static final String DATABASE_NAME="library.db";
    //数据库中三个最基本的表（读者、图书、借书）的名称
    private static final String READERS="readers";
    private static final String BOOKS="books";
    private static final String BORROWS="borrows";
    public LibraryDBHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase){
        String CREATE_READERS="createtablereaders("+"reader_idintegerprimarykey,"+"reader_numbertext,"+"reader_nametext,"+"reader_typetext,"+"reader_phonetext,"+"reader_passwordtext,"+"reader_createtimetext)";
        sqLiteDatabase.execSQL(CREATE_READERS);
        String CREATE_BOOK="createtablebooks("+"book_idintegerprimarykey,"+"book_nametext,"+"book_authortext,"+"book_publishertext,"+"book_intimetext,"+"book_countsinteger)";
        sqLiteDatabase.execSQL(CREATE_BOOK);
    }
    public void insert_reader(Reader reader){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        Log.d("文件位置",sqLiteDatabase.getPath());
        ContentValues values=new ContentValues();
        values.put("reader_number",reader.getReader_number());
        values.put("reader_name",reader.getReader_name());
        values.put("reader_type",reader.getReader_type());
        values.put("reader_phone",reader.getReader_phone());
        values.put("reader_createtime",reader.getReader_createtime());
        values.put("reader_password",reader.getReader_password());
        sqLiteDatabase.insert("readers",null,values);
        sqLiteDatabase.close();
    }
    public int delete_reader(String reader_num){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        int result=sqLiteDatabase.delete("readers","reader_number=?",new String[]{reader_num});
        sqLiteDatabase.close();
        return result;

    }
    public int update_reader(Reader reader){
        SQLiteDatabase sqLiteDatabase=getWritableDatabase();
        String reader_num=reader.getReader_number();
        ContentValues values=new ContentValues();
        values.put("reader_name",reader.getReader_name());
        values.put("reader_type",reader.getReader_type());
        values.put("reader_phone",reader.getReader_phone());
        values.put("reader_createtime",reader.getReader_createtime());
        values.put("reader_password",reader.getReader_password());
        int result=sqLiteDatabase.update("readers",values,"reader_number=?",new String[]{reader_num});
        sqLiteDatabase.close();
        return result;

    }
    @SuppressLint("Range")
    public Cursor query_reader(String reader_num){
        SQLiteDatabase sqLiteDatabase=getReadableDatabase();
        Cursor cursor=sqLiteDatabase.query("readers",null,"reader_numberLIKE?",new String[]{"%"+reader_num+"%"},null,null,null,null);
        return cursor;
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase,int i,int i1){

    }


}
