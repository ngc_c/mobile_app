package com.example.myapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

public class ListAdapter extends BaseAdapter {
    private Context mContext;
    DBHelper db = null;
    List<costList> mList;

    public ListAdapter(List<costList> list) {
        mList = list;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private List<costList> getmList;
    private LayoutInflater mLayoutInflater;

    public ListAdapter(Context context, List<costList> list) {
        mList = list;
        //通过外部传来的Context初始化LayoutInflater对象
        mLayoutInflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = mLayoutInflater.inflate(R.layout.list_item, null);
        //取出数据赋值
        costList item = mList.get(position);
        TextView tv_title = (TextView) view.findViewById(R.id.tv_title);
        TextView tv_date = (TextView) view.findViewById(R.id.tv_date);
        TextView tv_money = (TextView) view.findViewById(R.id.tv_money);
        //绑定
        tv_title.setText(mList.get(position).getTitle());
        tv_date.setText(mList.get(position).getDate());
        tv_money.setText(mList.get(position).getMoney());

        costList data = mList.get(position);
        if (data == null){
            data = new costList();
        }
        db = new DBHelper(mContext);
        final String removetitle =data.getTitle();
        //final String removedate = data.getDate();
        //final String removemoney =data.getMoney();
        //final  String removeid =data.get_id();

        final int removepos = position;

        Button deleteButton =(Button) view.findViewById(R.id.showDeleteButton);
        deleteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){

                final long temp = position;

                mList.remove(removepos);
                //db.delete_account(removetitle);
                //db.del(removepos);
                //db.del((int) temp);
                //db.delete_account(removedate);
                //db.delete_account(removemoney);
                //db.delete_account(removeid);
                notifyDataSetInvalidated();
            }
        });
        return view;
    }



}
