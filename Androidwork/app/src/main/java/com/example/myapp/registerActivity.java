package com.example.myapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class registerActivity extends AppCompatActivity {

    private TextView textView;
    private Button button1;
    private EditText editText1,editText2,editText3;
    private Spinner spinner1,spinner2;
    userdataDBHelper dbHelper = null;
    userdata username1 = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        dbHelper = new userdataDBHelper(getApplicationContext());

        editText1 = (EditText) findViewById(R.id.edit1);//name
        editText2 = (EditText) findViewById(R.id.edit2);//password
        editText3 = (EditText) findViewById(R.id.edit4);//iphone

        spinner1 = (Spinner) findViewById(R.id.spinner1);
        spinner2 = (Spinner) findViewById(R.id.spinner2);

        textView = (TextView)findViewById(R.id.resultTxt);

        String[] str = new String[]{"工作账本","恋爱账本","生活账本"};
        String[] grade = new String[]{"男","女"};

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(registerActivity.this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,str);
        ArrayAdapter<String> adaptergrade = new ArrayAdapter<String>(registerActivity.this, androidx.appcompat.R.layout.support_simple_spinner_dropdown_item,grade);


        adapter.setDropDownViewResource(androidx.constraintlayout.widget.R.layout.support_simple_spinner_dropdown_item);
        adaptergrade.setDropDownViewResource(androidx.constraintlayout.widget.R.layout.support_simple_spinner_dropdown_item );

        spinner1.setAdapter(adapter);//大学
        spinner2.setAdapter(adaptergrade);//年级
        Toast.makeText(registerActivity.this, spinner1.getSelectedItem().toString(), Toast.LENGTH_SHORT).show();

        button1 = (Button) findViewById(R.id.but1);
        button1.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @SuppressLint("Range")
            @Override
            public void onClick(View view) {
                userdataDBHelper dbHelper = new userdataDBHelper(getApplicationContext());
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd"+"HH:mm:ss");
                String name = editText1.getText().toString();
                String school = spinner1.getSelectedItem().toString();
                String grade = spinner2.getSelectedItem().toString();
                String iphone = editText3.getText().toString();
                String password = editText2.getText().toString();

                username1 = new userdata(name,school,grade,iphone,password);
                dbHelper.insert_user(username1);

                Intent intent = new Intent(registerActivity.this,LoginActivity.class);
                startActivityForResult(intent,1);
                Toast.makeText(registerActivity.this, "注册成功", Toast.LENGTH_SHORT).show();

            }

        });
    }
}