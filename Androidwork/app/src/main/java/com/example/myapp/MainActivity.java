package com.example.myapp;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends Activity {

    private DBHelper helper;
    private ListView listView;
    private ImageButton Add;
    private List<costList>list;
    private ImageView meImage,homeImage;
    private LinearLayout meLayout,homeLayout;
    private TextView homeText,meText,toolbartotle;
    private ViewPager viewPager;


    @RequiresApi(api = Build.VERSION_CODES.O)


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initView();
        initData();

        View v =findViewById(R.id.add);//修改透明度但未成功
        v.getBackground().setAlpha(250);

        //meLayout.setOnClickListener((View.OnClickListener) this);
        //homeLayout.setOnClickListener((View.OnClickListener) this);
        //viewPager.setOnCapturedPointerListener((View.OnCapturedPointerListener) this);
    };
    //初始化
    private void initData() {
        list=new ArrayList<>();
        SQLiteDatabase db=helper.getReadableDatabase();
        Cursor cursor=db.query("account",null,null,null,null,
                null,null);
        while (cursor.moveToNext()){
            costList clist=new costList();//构造实例
            clist.set_id(cursor.getString(cursor.getColumnIndex("_id")));
            clist.setTitle(cursor.getString(cursor.getColumnIndex("Title")));
            clist.setDate(cursor.getString(cursor.getColumnIndex("Date")));
            clist.setMoney(cursor.getString(cursor.getColumnIndex("Money")));
            list.add(clist);
        }
        //绑定适配器
        listView.setAdapter(new ListAdapter(this,list));
        db.close();
    }


    private void initView() {
        helper=new DBHelper(MainActivity.this);
        listView = (ListView) findViewById(R.id.list_view);
        Add= (ImageButton) findViewById(R.id.add);

        meImage = (ImageView) findViewById(R.id.me_icon);
        meLayout = (LinearLayout) findViewById(R.id.me_layout);
        homeImage = (ImageView) findViewById(R.id.home_icon);
        homeLayout = (LinearLayout) findViewById(R.id.home_layout);

        //homeImage.setImageResource(R.drawable.home);
        //homeText.setTextColor(getResources().getColor(R.color.colorTextBlack));

    }


    //@Override
    //public void onClick(View view){
    //resumeTab();
    //  switch (view.getId()){
    //  case R.id.me_layout:
    //   meImage.setImageResource(R.drawable.me);
    //   meText.setTextColor(getResources().getColor(R.color.green));
    //Intent intent = new Intent(MainActivity.this,show.class);
    // toolbartotle.setText(R.string.app_name);
    //  viewPager.setCurrentItem(1);
    //  break;
    //  default:
    //   break;
    // }
    // }

    //事件：添加
    public void addAccount(View view){//跳转
        Intent intent=new Intent(MainActivity.this,new_cost.class);
        startActivityForResult(intent,1);
    }

    public void show(View view){
        Intent intent = new Intent(MainActivity.this,title_add.class);
        startActivityForResult(intent,1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1&&resultCode==1)
        {
            this.initData();
        }
    }

}