package com.example.myapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class LoginActivity extends AppCompatActivity {

    private EditText editText1,editText2;
    private Button button1;
    private Button button2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        editText1 = (EditText) findViewById(R.id.edit11);
        editText2 = (EditText) findViewById(R.id.edit22);
        button2 = (Button) findViewById(R.id.zcbut);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this,registerActivity.class);
                startActivityForResult(intent,1);
            }
        });

        button1 = (Button) findViewById(R.id.loginbut);
        button1.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("Range")
            @Override
            public void onClick(View view) {
                userdataDBHelper DPBhelper = new userdataDBHelper(getApplicationContext());
                String name = editText1.getText().toString().trim();
                String password = editText2.getText().toString().trim();
                Cursor cursor = DPBhelper.query_user(name);
                int count = cursor.getCount();
                if (count == 0) {
                    Toast.makeText(LoginActivity.this, "用户名或密码不正确", Toast.LENGTH_SHORT).show();
                } else {
                    String str = "";
                    int i = 1;
                    while (cursor.moveToNext()) {
                        if (cursor.getString(cursor.getColumnIndex("iphone")).equals(name) && cursor.getString(cursor.getColumnIndex("password")).equals(password)) {
                            Toast.makeText(LoginActivity.this, "登录成功", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                            startActivity(intent);
                            break;
                        }
                        if (cursor.getString(cursor.getColumnIndex("iphone")).equals(name) && !cursor.getString(cursor.getColumnIndex("password")).equals(password)) {
                            Toast.makeText(LoginActivity.this, "用户名或密码不正确", Toast.LENGTH_SHORT).show();

                            break;
                        }

                    }
                    Toast.makeText(LoginActivity.this, "用户名或密码不能为空", Toast.LENGTH_SHORT).show();
                }


            }
        });

    }
}