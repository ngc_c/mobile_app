package com.example.myapp;

import java.io.Serializable;

/**
 * Created by MSI-PC on 2022/6/2.
 */

public class userdata implements Serializable {
    private String name;
    private String shcool;
    private String grade;
    private String iphone;
    private String password;
    private String cc;
    private String num;

    public userdata(){

    }
    public userdata(String name, String shcool, String grade, String iphone, String password){
        this.name=name;
        this.shcool=shcool;
        this.grade=grade;
        this.iphone=iphone;
        this.password = password;
        this.cc = cc;
        this.num =num;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIphone() {
        return iphone;
    }

    public void setIphone(String iphone) {
        this.iphone = iphone;
    }

    public String getCc() {
        return cc;
    }
    public void setCc(String title) {
        this.cc = title;
    }

    public String getNum() {
        return cc;
    }
    public void setNum(String num) {
        this.num = num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getShcool() {
        return shcool;
    }

    public void setShcool(String shcool) {
        this.shcool = shcool;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }
}
