package com.example.myapp;
import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by MSI-PC on 2022/6/2.
 */

public class userdataDBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION =1;
    private static final String DATABASE_NAME = "users.db";
    private static final String TBL_NAME = "usernames";
    private SQLiteDatabase db;


    final String DBusers = "create table usernames(" +
            "name integer primary key," +
            "shcool text," +
            "grade text," +
            "iphone text," +
            "password text," +
            "cc text," +
            "num text)";

    public userdataDBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version, @Nullable DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    public userdataDBHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DBusers);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
    public void insert_user(userdata username){
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name",username.getName());
        values.put("shcool",username.getShcool());
        values.put("grade",username.getGrade());
        values.put("iphone",username.getIphone());
        values.put("password",username.getPassword());
        values.put("cc",username.getCc());
        values.put("num",username.getNum());

        sqLiteDatabase.insert("usernames",null,values);
        sqLiteDatabase.close();
    }

    public int delete_user(String name){
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        int result = sqLiteDatabase.delete("usernames","name=?",
                new String[]{name});
        return result;
    }

    public int update_user(userdata username){
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        String name = username.getName();
        ContentValues values = new ContentValues();
        values.put("name",username.getName());
        values.put("shcool",username.getShcool());
        values.put("grade",username.getGrade());
        values.put("iphone",username.getIphone());
        values.put("password",username.getPassword());

        int result = sqLiteDatabase.update("usernames",values,"name=?",new String[]{name});
        sqLiteDatabase.close();
        return  result;

    }

    public void insert(ContentValues values) {
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TBL_NAME, null, values);
        db.close();
    }

    public Cursor query_user(String name){

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        Cursor cursor = sqLiteDatabase.query("usernames",null,"name LIKE ?",new String[]{"%"+name+"%"},
                null,null,null,null);
        return cursor;

    }


    @SuppressLint("Range")
    public ArrayList<userdata> getAllDATA(){
        ArrayList<userdata> list = new ArrayList<userdata>();
        Cursor cursor = db.query("user",null,null,null,null,null,"name DESC");
        while(cursor.moveToNext()){
            String name = cursor.getString(cursor.getColumnIndex("name"));
            String shcool = cursor.getString(cursor.getColumnIndex("shcool"));
            String grade = cursor.getString(cursor.getColumnIndex("grade"));
            String iphone = cursor.getString(cursor.getColumnIndex("iphone"));
            String password = cursor.getString(cursor.getColumnIndex("password"));
            list.add(new userdata(name,shcool,grade,iphone,password));
        }
        return list;
    }

    public Cursor query() {
        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TBL_NAME, null,
                null, null,
                null, null, null);
        return cursor;
    }
}