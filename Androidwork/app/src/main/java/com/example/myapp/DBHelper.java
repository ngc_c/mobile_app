package com.example.myapp;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.Date;

public class DBHelper extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;
    private static final String DB_NAME = "account_daily.db";
    private static final String TBL_NAME = "account";
    private SQLiteDatabase stuDB;

    public DBHelper(Context context) {
        super(context, DB_NAME ,null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String sql="create table account(" +
                "_id integer primary key autoincrement," +//id
                "Title varchar(20) ," +//Title
                "Date varchar(20)," +//Date
                "Money varchar(20))";//Money
        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public void inser_plan(Date Title){
        SQLiteDatabase sqLiteDatabase =getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("Title",Title.getTime());
        values.put("Money",Title.getTime());
        sqLiteDatabase.insert("account",null,values);
        sqLiteDatabase.close();
    }

    public int delete_account(String Title){
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        int result = sqLiteDatabase.delete("account","_id=?",
                new String[]{Title});
        //sqLiteDatabase.close();
        return  result;
    }
    public void del(int id) {
        if (stuDB == null)
            stuDB = getWritableDatabase();
        stuDB.delete(TBL_NAME, "_id?", new String[]{String.valueOf(id)});
    }

}