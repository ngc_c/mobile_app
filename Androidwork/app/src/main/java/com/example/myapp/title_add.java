package com.example.myapp;

import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class title_add extends AppCompatActivity {
    private EditText studentNumEdt,studentNameEdt;
    textDBHelper dbHelper;
    private Button studentAddBtn,title;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_title_add);

        this.setTitle("记事本：新增记录");

        dbHelper = new textDBHelper(this);

        textDBHelper helper=new textDBHelper(getApplicationContext());

        studentAddBtn=(Button) findViewById(R.id.but9);
        studentNumEdt=(EditText) findViewById(R.id.edit9);
        studentNameEdt=(EditText) findViewById(R.id.edit8);
        title=(Button)findViewById(R.id.titel);

        studentAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = studentNumEdt.getText().toString();
                String num =studentNameEdt.getText().toString();

                ContentValues values=new ContentValues();
                values.put("name",title);
                values.put("num",num);

                textDBHelper helper =new textDBHelper(getApplicationContext());
                helper.insert(values);
                studentNameEdt.setText("");
                studentNumEdt.setText("");
            }
        });

        title.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(title_add.this,show.class);
                startActivity(intent);
            }
        });
    }

}