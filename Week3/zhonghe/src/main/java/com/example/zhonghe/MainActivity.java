package com.example.zhonghe;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
    private EditText xuehao;
    private EditText mima;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        xuehao=(EditText)findViewById(R.id.ButtonB);
        mima=(EditText)findViewById(R.id.shuru2);
        button=(Button)findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View view)
            {
                Bundle bundle =new Bundle();
                bundle.putInt("num",1);
                bundle.putString("msg1",xuehao.getText().toString());
                bundle.putString("msg2",mima.getText().toString());

                String text1=xuehao.getText().toString();
                String text2=mima.getText().toString();
                if(text1.equals("201958501303")&&text2.equals("123456"))
                {
                    Intent intent =new Intent(MainActivity.this,SencondActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
                else
                    Toast.makeText(getApplicationContext(),"密码错误",Toast.LENGTH_LONG).show();
            }
        }
        );
    }
}
