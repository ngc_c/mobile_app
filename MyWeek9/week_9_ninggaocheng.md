# 第 9 周 《手机移动开发》实践报告 
### 实践目的 
1. 能够运用Spinner和ListView构造应用程序
1. 能够使用SQLite数据库，完成简单的信息管理系统中数据的增删改查操作；
1. 运用Spinner组件与ListView组件
### 实践内容 1：建立模块及主控界面
#### 模块名 
app
#### 完成的主要功能 
1. 下拉列表Spinner，体验使用Spinner实现简单下拉列表和列表对话框
1. 列表视图ListView，通过继承ListView类来使用列表视图
1. 在Activity中自定义ListView
### 实践内容 2：复杂ListView
#### 模块名 
app
#### 完成的主要功能 
1. 自定义实现一个图片和文字混合的列表；
1. 文件浏览器，实现手机端的文件浏览器，可以显示文件/文件夹列表，查看下一级，加到上一级等；
1. 学生数据库，实现查询和删除学生的处理；
#### 完成过程中遇到的问题及解决办法 
1. 写到ListView2时由于版本问题需要将extends AppCompatActivity修改成extends；
#### 完成该实践项目的收获及感想 
1. 任何问题都可以解决，我们只要足够上心任何事情都可以找到解决的办法。
1. 要跟老师同学们虚心请教。
