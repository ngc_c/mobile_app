package com.example.msi_pc.myweek9;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class StudentAdd_Activity extends AppCompatActivity {
    //定义组件
    private EditText studentNumEdt,studentNameEdt;
    private Button studentAddBtn, studentQueryBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_student_add_);
//设置标题栏
        this.setTitle("添加收藏信息");
//初始化组件
        studentNumEdt= (EditText)findViewById(R.id.studentNumEdt);
        studentNameEdt= (EditText)findViewById(R.id.studentNameEdt);
        studentAddBtn= (Button)findViewById(R.id.studentAddBtn);
        studentQueryBtn= (Button)findViewById(R.id.studentQueryBtn);
//定义“添加”按钮的事件监听器
        studentAddBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//获取用户输入的文本信息
                String name=studentNameEdt.getText().toString();
                String num=studentNumEdt.getText().toString();
//创建 ContentValues 对象，封装记录信息
                ContentValues values=new ContentValues();
                values.put("name",name);
                values.put("num",num);
//创建数据库工具类 StudentDBHelper 的对象，为访问数据库准备
                StudentDBHelper helper=new StudentDBHelper(getApplicationContext());
//调用 StudentDBHelper 类中定义的 insert()方法插入数据
                helper.insert(values);
//清空编辑框并提示“添加成功”
                studentNameEdt.setText("");
                studentNumEdt.setText("");
                Toast.makeText(StudentAdd_Activity.this, "添加成功",
                        Toast.LENGTH_SHORT).show();
            }
        });
//定义“查询”按钮的事件监听器
        studentQueryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//跳转到 StudentQueryActivity，显示学生列表
                Intent intent=new Intent(StudentAdd_Activity.this,StudentQuery_Activity.class);
                startActivity(intent);
            }
        });
    }
}