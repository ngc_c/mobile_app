package com.example.msi_pc.myweek9;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

public class SpinnerActivity extends AppCompatActivity {
    //定义学习场所下拉列表需要显示的文本数组,文本数组可以直接作为Adapter的数据来源
    private String[] learningSiteArray={
            "逸夫图书馆","承先图书馆","综合楼","四教","五教","六教"
    };
    //定义运动场所对话框列表需要显示的文本数组,该数组内容将由资源文件填充
    private String[] playSiteArray;

    //用于显示结果的文本框组件
    private TextView resultText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner);

        //初始化组件
        resultText= (TextView) findViewById(R.id.sp_result);
        //初始化下拉模式的列表框
        intiDropdownSpinner();
        //初始化对话框样式的列表框
        intiDialogSpinner();
    }
    //初始化下拉模式的列表框
    private void intiDropdownSpinner() {
        Spinner sp_dropdown= (Spinner) findViewById(R.id.sp_dropdown);
        sp_dropdown.setPrompt("请选择学习场所");
        //先声明一个下拉列表的数组适配器，指定下拉列表
        ArrayAdapter<String> learningSiteAdapter=new ArrayAdapter<String>(this,R.layout.item_select,learningSiteArray);
        sp_dropdown.setAdapter(learningSiteAdapter);//设置下拉框的数组适配器
        sp_dropdown.setSelection(0);//设置下拉框默认显示第一项
        sp_dropdown.setOnItemSelectedListener(onItemSelectedListener);

    }

    //初始化对话框样式的列表框
    private void intiDialogSpinner() {
        playSiteArray=getResources().getStringArray(R.array.palySites);
        Spinner sp_dialog= (Spinner) findViewById(R.id.sp_dialog);
        sp_dialog.setPrompt("请选择运动场所");
        ArrayAdapter<String>palySiteAdapter=new ArrayAdapter<String>(this,R.layout.item_select,playSiteArray);
        sp_dialog.setAdapter(palySiteAdapter);//设置下拉框的数组适配器
        sp_dialog.setOnItemSelectedListener(onItemSelectedListener);
    }
    //定义一个选择监听器，它实现了接口OnItemSelectedListener
    private AdapterView.OnItemSelectedListener onItemSelectedListener=new AdapterView.OnItemSelectedListener(){
        @Override
        public void onItemSelected(AdapterView<?>adapterView, View view, int position, long l){
            //选择事件的处理方法，其中position代表选择项的序号
            switch(adapterView.getId()){
                case R.id.sp_dropdown:
                    resultText.setText("学习在"+learningSiteArray[position]);
                    break;
                case R.id.sp_dialog:
                    resultText.setText("运动在"+playSiteArray[position]);
                    break;
            }
        }
        //未选择时的处理方法，通常无需关注
        @Override
        public void onNothingSelected(AdapterView<?>adapterView){

        }
    };
}
