package com.example.msi_pc.myweek9;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListView3_Activity extends AppCompatActivity {

    private String[]learningSiteArray={"逸夫图书馆","承先图书馆","综合楼","四教","五教","六教"};
    ArrayList arrayList=new ArrayList();
    ListView listView=null;
    EditText addEdit;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view3_);
        for (int i=0;i<learningSiteArray.length;i++){
            arrayList.add(learningSiteArray[i]);
        }
        listView= (ListView) findViewById(R.id.listview3);
        final ArrayAdapter adapter=new ArrayAdapter<String>(this,android.R.layout.simple_expandable_list_item_1,arrayList);
        listView.setAdapter(adapter);
        //设置ListView中数据项被点击的事件监听
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                            Toast.makeText(ListView3_Activity.this,
                                                    "您选择了" + learningSiteArray[position], Toast.LENGTH_LONG)
                                                    .show();
                                        }
                                    });
        //初始化处理“添加”逻辑的组件
        addEdit= (EditText) findViewById(R.id.addListView3Edt);
        //设置”添加“按钮的事件监听
        findViewById(R.id.addListView3Bnt).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //向arrayList数组中增加一个元素
               arrayList.add(addEdit.getText().toString());
                //通知Adapter对象刷新数据集（这一步很关键）
                adapter.notifyDataSetInvalidated();
                //清空编辑框
                addEdit.setText("");

            }
        });

    }
}
