package com.example.msi_pc.myweek9;

import android.app.ListActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class ListView2_Activity extends ListActivity {
    //定义下拉列表需要显示的文本数组
    private String[]learningSiteArray={"逸夫图书馆","承先图书馆","综合楼","四教","五教","六教"};
    //为方便后面可以进行”添加“等动态操作，不能用静态数组做数据源，而要用动态的ArrayList表示数据
    ArrayList arrayList=new ArrayList();

    //定义ListView对象
    ListView listView=null;

    //定义用于”添加“的编辑框对象
    EditText addEdt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view2_);

        //从静态数组中获得列表项
        // 请自行拓展，尝试从文件、SharedPreferences、数据库中动态获得数据
        for(int i=0;i<learningSiteArray.length;i++){
            arrayList.add(learningSiteArray[i]);
        }
        //获得系统默认的ListView组件
        listView=getListView();

        //创建Adapter对象，因为其后要实现数据源的更新，
        // 故不能像教材中那样用匿名对象，也不能使用固定大小的数组
        final ArrayAdapter adapter=new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1,arrayList);
        /*AndroidAPI中默认提供了一些的用于设置外观的布局文件，用于方便地指定ListView的外观，包括：
        simple_list_item_1：每个列表项都是一个普通的文本
        simple_list_item_2：每个列表项都是一个普通的文本（字体略大）
        simple_list_item_checked：每个列表项都有一个已选中的列表项
        simple_list_item_multiple_choice：每个列表项都是一个带复选框的文本
        simple_list_item_single_choice：每个列表项都是一个单选按钮的文本在实践时可以将
        simple_list_item_1换为这些选项查看结果
        */
        //为系统默认的ListView设置Adapter对象
        setListAdapter(adapter);

        //设置ListView中数据项被点击的事件监听
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
            @Override
            public void onItemClick(AdapterView<?>parent, View view,
                                    int position, long id){
                Toast.makeText(ListView2_Activity.this,
                        "您选择了"+learningSiteArray[position],Toast.LENGTH_LONG)
                        .show();
            }
        });
        //初始化组件
        addEdt= (EditText) findViewById(R.id.addListView2Edt);
        //设置”添加“按钮的事件监听
        findViewById(R.id.addListView2Btn).setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                arrayList.add(addEdt.getText().toString());
                //通知Adapter对象刷新数据集（这一步很关键）
                adapter.notifyDataSetInvalidated();
            }
        });
    }
}