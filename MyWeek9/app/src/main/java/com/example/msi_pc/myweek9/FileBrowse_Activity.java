package com.example.msi_pc.myweek9;

import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FileBrowse_Activity extends AppCompatActivity {

    //定义组件
    ListView listView;
    TextView textView;
    //记录当前的父文件夹
    File currentParent;
    //记录当前路径下所有文件的文件数组
    File[] currentFiles;
    @Override
    protected void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_file_browse_);
//初始化组件：获取列出全部文件的 ListView
        listView= (ListView) findViewById(R.id.fileList);
        textView= (TextView) findViewById(R.id.filePath);
//获取系统的 SD 卡目录
        File root = Environment.getExternalStorageDirectory();
//如果 SD 卡存在
        if(root.exists()){
//当前目目录为 SD 卡的根目录
            currentParent=root;
            currentFiles=root.listFiles();
//调用自定义函数（见后定义），用当前目录下的全部文件、文件夹来填充 ListView
            inflateListView(currentFiles);
        }
//为 ListView 的列表项的单击事件绑定监听器
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
//用户单击了文件，直接返回，不做任何处理
                if(currentFiles[position].isFile())
                    return;
//若未返回，点击的必是文件夹；将首先获取用户点击的文件夹下的所有文件
                File[] tmp=currentFiles[position].listFiles();
//根据取得的文件夹状态分情况处理
                if(tmp==null || tmp.length==0){ //若该文件夹为空
                    Toast.makeText(FileBrowse_Activity.this,
                            "当前路径不可访问或该路径下没有文件",
                            Toast.LENGTH_LONG)
                            .show();
                }else{ //当文件夹不为空时
//将用户单击的列表项对应的文件夹设为当前的父文件夹
                    currentParent =currentFiles[position];
//保存当前的父文件夹内的全部文件和文件夹（两个文件对象数组直接赋值）
                    currentFiles=tmp;
//再次更新 ListView
                    inflateListView(currentFiles);
                }
//获取上一级目录的按钮
                Button parent= (Button) findViewById(R.id.fileParent);
//为返回上一级目录按钮设置事件监听程序
                parent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        try{
//若当前目录不是根目录，则回到其上一级目录（否则不变）
                            if(!currentParent.getCanonicalPath().equals("/storage/emulated/0")){
//获取上一级目录
                                currentParent=currentParent.getParentFile();
//列出当前目录下所有文件
                                currentFiles=currentParent.listFiles();
//调用自定义函数，更新 ListView
                                inflateListView(currentFiles);
                            }
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        });
    }
    //定义内部的自定义函数，用于列出所有文件夹和文件
//参数：files - 要去显示的文件夹和文件数组
    private void inflateListView(File[] files){
//创建一个 List 集合，用作为适配器中的数据源。List 集合的元素是 Map。
        List<Map<String,Object>> listItems=new ArrayList<Map<String, Object>>();
//组装将用于适配器数据源的 List 集合
        for(int i =0; i<files.length;i++){
//新建一个 Map 对象
            Map<String,Object> listItem= new HashMap<>();
//为新建的 Map 对象置初值
//如果当前 File 是文件夹，使用 folder 图标；否则使用 file 图标
            if(files[i].isDirectory()){
                listItem.put("icon",R.drawable.folder);
            }else {
                listItem.put("icon",R.drawable.file);
            }
//记录文件夹或文件的名字
            listItem.put("fileName",files[i].getName());
//将 map 作为 list 项添加到 List 集合中
            listItems.add(listItem);
        }
//创建一个 SimpleAdapter
        SimpleAdapter simpleAdapter=new SimpleAdapter(this,
                listItems,
                R.layout.fileitem,
                new String[]{"icon","fileName"},
                new int[]{R.id.fileIcon,R.id.file_name});
//显示当前路径
        try {
            textView.setText("当前路径为："+currentParent.getCanonicalPath());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}