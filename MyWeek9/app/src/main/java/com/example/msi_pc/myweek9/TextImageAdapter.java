package com.example.msi_pc.myweek9;

import android.content.Context;
import android.support.v7.view.menu.MenuView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by MSI-PC on 2022/4/27.
 */

public class TextImageAdapter extends BaseAdapter{
    private Context mContext;
    //展示的文字
    private List<String> texts;
    //图片
    private List<Integer>images;
    //构造
    public TextImageAdapter(Context mContext,List<String>texts,List<Integer>images){
        this.mContext=mContext;
        this.texts=texts;
        this.images=images;
    }
    //元素的个数
    @Override
    public int getCount(){
        return texts.size();
    }
    //取得
    @Override
    public Object getItem(int i){
        return null;
    }
    @Override
    public long getItemId(int i){
        return 0;
    }

    //用于生成在Listview中展示的一个View元素
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext)
                    .inflate(R.layout.item, null);
            ItemViewCache viewCache = new ItemViewCache();
            viewCache.mTextView = (TextView) convertView
                    .findViewById(R.id.itemListView4Txt);
            viewCache.mImageView = (ImageView) convertView
                    .findViewById(R.id.itemListView4Img);
            convertView.setTag(viewCache);
        }
        ItemViewCache cache = (ItemViewCache) convertView.getTag();
// 设置文本和图片，然后返回这个 View，用于 ListView 的 Item 的展示
        cache.mTextView.setText(texts.get(position));
        cache.mImageView.setImageResource(images.get(position));
        return convertView;
    }
    //创建内部私有类
    private class ItemViewCache {
        public TextView mTextView;
        public ImageView mImageView;
    }
}
