package com.example.msi_pc.myapplication;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity
{
    private Button clickBten;
    private TextView showText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        showText=(TextView) findViewById(R.id.showText);
        clickBten=(Button) findViewById(R.id.clickBtn);

        clickBten.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                showText.setText("按钮被单击");
            }
        });
        showText.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(MainActivity.this,"文本框被点了",Toast.LENGTH_LONG).show();
            }
        });
    }
}

