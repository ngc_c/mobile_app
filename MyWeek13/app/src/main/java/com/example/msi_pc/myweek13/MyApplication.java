package com.example.msi_pc.myweek13;

import android.app.Application;

/**
 * Created by MSI-PC on 2022/5/23.
 */

public class MyApplication extends Application {
    //在MyApplication中提供的属性将是整个应用中的公共全局变量（公共全局常量在资源文件中定义）
    //本例中，以用户的姓名和所在机构作为全局变量的代表，这两个变量在后面很多功能中都要用到
    private String userName;
    private String orgName;

    //点菜单Code-->Override Methods...，选择重载onCreate()方法
    //此例中初始化这些全局变量
    //此处采取了直接赋值的方法，在实际应用中，可以采用数据存储方案永久保存（思考：益处是什么？）
    @Override
    public void onCreate() {
        super.onCreate();
        setUserName("anonymous");
        setOrgName("unknown");
    }
    public String getUserName(){
        return userName;
    }
    public void setUserName(String userName){
        this.userName=userName;
    }
    public  String getOrgName(){
        return orgName;
    }
    public void setOrgName(String orgName){
        this.orgName=orgName;
    }



}