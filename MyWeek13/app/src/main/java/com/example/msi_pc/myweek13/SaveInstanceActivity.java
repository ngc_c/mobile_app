package com.example.msi_pc.myweek13;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class SaveInstanceActivity extends AppCompatActivity {


    //定义用于保存数据的Bundle对象的键值常量
    private final String GAME_STATE_KET="gameState";
    private final String GAME_CREDITS_KET="gameCredits";


    private MyApplication app;

    private TextView global_variable_txt;

    //定义当前页面中的组件
    private EditText game_credits_edt;
    //定义当前类中的“局部变量”
    //gameState作为本类中的私有成员的代表，用于演示如何被保存和恢复
    String gameState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_save_instance);

        app=(MyApplication)getApplication();
        global_variable_txt= (TextView) findViewById(R.id.global_variable_txt);
        global_variable_txt.setText("用户姓名:"+app.getUserName()+"\n机构名称："+app.getOrgName());

    }
}
