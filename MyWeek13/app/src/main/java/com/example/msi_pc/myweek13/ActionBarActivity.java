package com.example.msi_pc.myweek13;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class ActionBarActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_action_bar);
        //重新设置标题（替换默认标题）
        setTitle("首页");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu1,menu);
        return super.onCreateOptionsMenu(menu);
    }

    
    public boolean onOptionsltemSelected(@NonNull MenuItem item){
        switch (item.getItemId()){
            case R.id.search:
                Toast.makeText(this,"处理搜索功能",Toast.LENGTH_SHORT).show();
                break;
            case R.id.bell:
                Toast.makeText(this,"此处有提醒",Toast.LENGTH_SHORT).show();
                break;
            case R.id.settings:
            Toast.makeText(this,"设置系统环境",Toast.LENGTH_SHORT).show();
            break;
            case R.id.about:
            Toast.makeText(this,"先占位，再实现",Toast.LENGTH_SHORT).show();
            break;
        }
        return super.onOptionsItemSelected(item);
    }
}
