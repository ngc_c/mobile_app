# 第 5 周 《手机移动开发》实践报告 
### 实践目的 
1. 了解 ImageView、CompoundButton、Dialog 类族中的常用组件，并用于构造用户界面； 
1. 进一步熟练进行应用程序开发的一般流程和方法；
### 实践内容 1：Dialog 对话框 
#### 模块名 
univeersityflags 
#### 完成的主要功能 
1. 改造应用，在第一页向前、末一页向后时，不再使用 Toast，而是改用 AlertDialog 来提示。
1. 改变布局及在其中增加组件
1. 使用字符串资源
#### 完成过程中遇到的问题及解决办法 
1. 复选框中代码问题因为版本不同通过点击小灯泡解决 
#### 完成该实践项目的收获及感想 
1. 任何问题都可以解决，需要我们上心任何事情都可以找到解决的办法
1. 要跟老师同学们虚心请教
### 实践内容 2：图片式开关 
#### 模块名 
univeersityflags 
#### 完成的主要功能 
1. 通过在 Switch 组件上加文字和图片，实现更好的效果。将通过开关组件，控制 校徽的大小；
1. 用图片的形式显示 ToggleButton 组件；
1. 控制是否要在界面中显示日 期时间； 
#### 完成过程中遇到的问题及解决办法 
1. 在 drawable 文件夹中名为 selector_btn_toggle.xml 的文件该问题可以通过新建一个文本文件再修改粘贴进drawable来解决；
#### 完成该实践项目的收获及感想 
1. 积极听课争取当堂完成代码问题
